
# README #

Welcome to the Pathzero Frontend Test!

![Pathzero UI](/pathzero-test.jpg)

### The brief ###
Using your favourite UI framework and tools, build the UI represented in `/pathzero-test.jpg`.

Fetch your data from the following mock endpoint:

[https://60b433144ecdc10017480478.mockapi.io/api/v1/emissions](https://60b433144ecdc10017480478.mockapi.io/api/v1/emissions)

The mock endpoint returns a list of objects. Each object property maps to an item's name, description and CO2 emissions. The "progress bar" is the percentage of that item's emissions, relative to the total emissions.

The "sorting" functionality should have two options: "Highest - Lowest" (default) and "Lowest - Highest". This refers to the emissions for each item.

The right hand side widget should show the total emissions of all items; and a breakdown of the top 5 highest emitting items.

The "CTA" buttons don't need to click through to anywhere.

### Assessement ###
You will be assessed for your ability to dissect the UI and build in a way that makes each component independent and reusable.

You do not need to match the UI pixel for pixel, but attention to detail is a plus.

Writing a few tests is a huge bonus - but not required due to time constraints.

### How to submit ###
Fork this repository & once you've completed your work give charbel@pathzero.com access to your repository for assessement.

### Assets ###
Refer to the `/assets` folder for any required assets.

And for the fonts, feel free to use the following:

```
@font-face {
	font-family: 'Sharp Sans';
	src: url('https://assets.pathzero.com/pathzero/fonts/sharp-sans/SharpSans-Book.woff2') format('woff2'),
		url('https://assets.pathzero.com/pathzero/fonts/sharp-sans/SharpSans-Book.woff') format('woff');
	font-weight: 400;
	font-style: normal;
	font-display: swap;
}

@font-face {
	font-family: 'Sharp Sans';
	src: url('https://assets.pathzero.com/pathzero/fonts/sharp-sans/SharpSans-Bold.woff2') format('woff2'),
		url('https://assets.pathzero.com/pathzero/fonts/sharp-sans/SharpSans-Bold.woff') format('woff');
	font-weight: 700;
	font-style: normal;
	font-display: swap;
}

@font-face {
	font-family: 'Sharp Sans';
	src: url('https://assets.pathzero.com/pathzero/fonts/sharp-sans/SharpSans-Extrabold.woff2') format('woff2'),
		url('https://assets.pathzero.com/pathzero/fonts/sharp-sans/SharpSans-Extrabold.woff') format('woff');
	font-weight: 800;
	font-style: normal;
	font-display: swap;
}
```